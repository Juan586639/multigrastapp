using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemToSell : MonoBehaviour
{
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI itemPrice;
    public TextMeshProUGUI itemCategory;
    public Image itemImg;
    public void SetupData(string iname, int iprice, Sprite sprite, string iCategory)
    {
        itemImg.sprite = sprite;
        itemName.text = iname;
        itemPrice.text = iprice.ToString();
        itemCategory.text = iCategory;
    }
}
