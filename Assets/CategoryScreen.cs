using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CategoryScreen : MonoBehaviour
{
    public TextMeshProUGUI titleText;
    public Image categoryImg;
    public GameObject productPrefab;
    public Transform productsContainer;
    public ScrollRect scrollRect;
    private List<GameObject> itemsOnScreen = new List<GameObject>();
    public void SetupScreen(List<ProductData> products, Sprite cImage)
    {
        gameObject.SetActive(true);
        categoryImg.sprite = cImage;
        foreach (var item in products)
        {
            titleText.text = item.productCategory.GetAttributeOfType<StringValueAttribute>().StringValue;
            GameObject tempProduct = Instantiate(productPrefab, productsContainer);
            tempProduct.GetComponent<ItemToSell>().SetupData(item.productName, item.price, item.productImg, item.productCategory.GetAttributeOfType<StringValueAttribute>().StringValue);
            itemsOnScreen.Add(tempProduct);
        }
    }

    private void OnDisable()
    {
        scrollRect.verticalNormalizedPosition = 1;
        foreach (var item in itemsOnScreen)
        {
            Destroy(item.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
