using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ProductsManager : MonoBehaviour
{
    public Products products;
    public GameObject productPrefab;
    public Transform productsContainer;
    public CategoryScreen categories;

    private void Start()
    {
        foreach (var item in products.products)
        {
            if (item.isBestSeller)
            {
                GameObject tempProduct = Instantiate(productPrefab, productsContainer);
                tempProduct.GetComponent<ItemToSell>().SetupData(item.productName, item.price, item.productImg, item.productCategory.GetAttributeOfType<StringValueAttribute>().StringValue);
            }
        }
    }
    public void OpenBathroom()
    {
        SetCategoryScreen(ProductData.Category.bathroom);
    }
    public void OpenFloor()
    {
        SetCategoryScreen(ProductData.Category.floors);
    }
    public void OpenHouseCleaning()
    {
        SetCategoryScreen(ProductData.Category.houseCleaning);
    }
    public void OpenHouseKitchen()
    {
        SetCategoryScreen(ProductData.Category.kitchen);
    }
    public void SetCategoryScreen(ProductData.Category selectedCategory)
    {
        List<ProductData> matchedData = products.products.Where(p => p.productCategory == selectedCategory).ToList();
        categories.SetupScreen(matchedData, products.categoriesBanner[(int)matchedData[0].productCategory]);
    }
}
