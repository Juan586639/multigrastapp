using System;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class ProductData 
{
    [SerializeField]
    public Category productCategory;
    public enum Category
    {
        [StringValue("Ba�o")]
        bathroom = 0,
        [StringValue("Cocina")]
        kitchen = 1,
        [StringValue("Limpieza de hogar")]
        houseCleaning = 2,
        [StringValue("Pisos y superficies")]
        floors = 3,
    }
    [SerializeField]
    public bool isBestSeller;
    [SerializeField]
    public string productName;
    [SerializeField]
    public int price;
    public Sprite productImg; 

}