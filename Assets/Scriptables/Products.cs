using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Products", menuName = "ScriptableObjects/Products", order = 1)]
public class Products : ScriptableObject
{
    [SerializeField]
    public List<ProductData> products;
    [SerializeField]
    public List<Sprite> categoriesBanner;
}
